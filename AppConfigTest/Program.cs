﻿using System;
using System.Configuration;

namespace AppConfigTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string msg = ConfigurationManager.AppSettings["Message"];
            Console.WriteLine(msg);
            if (ConfigurationManager.AppSettings["ApiKey"] != null)
                Console.WriteLine(ConfigurationManager.AppSettings["ApiKey"]);
        }
    }
}